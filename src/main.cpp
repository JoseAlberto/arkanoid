#define UNUSED_VARIABLE(x) (void)x

#include "GameManager.h"
#include "RecordManager.h"
#include "MenuState.h"
#include "IntroState.h"
#include "PlayState.h"
#include "PauseState.h"
#include "EndState.h"

#include <iostream>

using namespace std;

int main(){

    /* Managers */
    GameManager* game = new GameManager();
    RecordManager* rm = new RecordManager();

    /* States */
    IntroState* introState = new IntroState();
    MenuState* menuState = new MenuState();
    PlayState* playState = new PlayState();
    PauseState* pauseState = new PauseState();
    EndState* endState = new EndState();
    
    UNUSED_VARIABLE(rm);
    UNUSED_VARIABLE(introState);
    UNUSED_VARIABLE(menuState);
    UNUSED_VARIABLE(playState);
    UNUSED_VARIABLE(pauseState);
    UNUSED_VARIABLE(endState);
        
    try{
        game->start(IntroState::getSingletonPtr());
    }catch (Ogre::Exception& e){
        std::cerr << "Excepción detectada: " << e.getFullDescription();
    }//Fin try-cath
      
    delete game;
      
    return 0;
}//Fin main
