#include "IntroState.h"
#include "MenuState.h"
#include "RecordManager.h"
#include <iostream>


template<> IntroState* Ogre::Singleton<IntroState>::msSingleton = 0;

void IntroState::enter (){
    _root = Ogre::Root::getSingletonPtr();


    _sceneMgr = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");

    _camera = _sceneMgr->createCamera("CamaraPrincipal");
    _camera->setPosition(Ogre::Vector3(0, 0, 0));
    _camera->setNearClipDistance(5);
    _camera->setFarClipDistance(10000);

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 0.0));

    _pTrackManager=TrackManager::getSingletonPtr();
    TrackPtr mainTrack;
    mainTrack = _pTrackManager->load("arkanoid.mp3");
    mainTrack->play(true);


    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();
    _camera->setAspectRatio(width / height);

    std::cout << "Cargando Records..." << std::endl;
    RecordManager::getSingletonPtr()->loadRecords();


    createScene();


    _exitGame = false;
}//Fin enter

void IntroState::createScene(){
    Ogre::Entity *ent = NULL;
    Ogre::SceneNode *nodo = NULL;
    

    _camera->setPosition(Ogre::Vector3(0, 0, 10));
    _camera->lookAt(Ogre::Vector3(0, 2, 0));
    
    nodo = _sceneMgr->getRootSceneNode()->createChildSceneNode(
        "Background", Ogre::Vector3(0, 0, -2));
    ent = _sceneMgr->createEntity("Background.mesh");
    nodo->attachObject(ent);
    
    nodo = _sceneMgr->getRootSceneNode()->createChildSceneNode(
        "Titulo", Ogre::Vector3(-0.5, 2, 0));
    ent = _sceneMgr->createEntity("Titulo.mesh");
    nodo->attachObject(ent);

    nodo = _sceneMgr->getRootSceneNode()->createChildSceneNode(
    "Pulsar", Ogre::Vector3(0, 0, 0));
    ent = _sceneMgr->createEntity("Mesh.mesh");
    nodo->attachObject(ent);
    nodo->yaw(Ogre::Degree(-90), Ogre::Node::TS_LOCAL);

    animState = ent->getAnimationState("Pulsar");
	animState->setEnabled(true);
	animState->setTimePosition(0);
	animState->setLoop(true);

    /* Iluminacion */
    Ogre::Light *light = _sceneMgr->createLight("Light");
    light->setType(Ogre::Light::LT_POINT);
    light->setPosition(0, 5, 5);
    light->setSpecularColour(0.9, 0.9, 0.9); 
    light->setDiffuseColour(0.9, 0.9, 0.9);



   // _simpleEffect = _pSoundFXManager->load("nightmare.wav");

}//Fin createScene

void IntroState::exit(){
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();

}//Fin exit

void IntroState::pause (){}

void IntroState::resume (){}

bool IntroState::frameStarted(const Ogre::FrameEvent& evt) {

	animState->addTime(evt.timeSinceLastFrame);
    return true;
}//Fin frameStarted

bool IntroState::frameEnded(const Ogre::FrameEvent& evt){
    if(_exitGame){
        return false;
    }//Fin if
  
    return true;
}//Fin if

void IntroState::keyPressed(const OIS::KeyEvent &e){
    /* Transición al siguiente estado.
     * Espacio --> MenuState */
    switch(e.key){
        case OIS::KC_SPACE:
//        	_mainTrack->stop();
//        	_pTrackManager->unload("arkanoid.mp3");
            changeState(MenuState::getSingletonPtr());
        default:
            /* Cualquier otra tecla no hace nada */
            break;
    }//Fin if
}//Fin if

void IntroState::keyReleased(const OIS::KeyEvent &e ){
    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    }//Fin if
}//Fin keyReleased

IntroState* IntroState::getSingletonPtr (){
    return msSingleton;
}//Fin getSingletonPtr

IntroState& IntroState::getSingleton (){ 
    assert(msSingleton);
    return *msSingleton;
}//Fin getSingleton



