#include "MenuState.h"
#include "PlayState.h"
#include "RecordManager.h"
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#include <iostream>

template<> MenuState* Ogre::Singleton<MenuState>::msSingleton = 0;

void MenuState::enter (){
    _root = Ogre::Root::getSingletonPtr();

    _sceneMgr = _root->getSceneManager("SceneManager");

    _camera = _sceneMgr->getCamera("CamaraPrincipal");

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 0.0));

    _pTrackManager = TrackManager::getSingletonPtr();
    _pSoundFXManager = SoundFXManager::getSingletonPtr();

    _simpleEffect = _pSoundFXManager->load("FXMovPointer.ogg");
    _pTrackManager->load("arkanoid.mp3");

    if(!_pTrackManager->load("arkanoid.mp3")->isPlaying()){
    	_pTrackManager->load("arkanoid.mp3")->play(true);
    }//Fin if

    createScene();
    createOverlay();

    _exitGame = false, _inOption = false;
}//Fin enter

void MenuState::createScene(){
    /* Variables que vamos a usar */
    Ogre::Entity *ent = NULL;
    _opNode = _sceneMgr->getRootSceneNode()->createChildSceneNode(
        "OpMenu", Ogre::Vector3(0, 0, 0));
    Ogre::SceneNode *nodoAux = NULL;

    std::stringstream act_name;
    Ogre::String sn_names[] = {"NGame", "Records", "About", "Quit"};
    //_simpleEffect=
    _pSoundFXManager->load("FXMovPointer.ogg");
    /* Iniciamos la variable de las opciones */
    _select = 0;
    _options = new Ogre::SceneNode*[4];

    /* Posicion y vision de la camara */
    _camera->setPosition(Ogre::Vector3(3, 2, 10));
    _camera->lookAt(Ogre::Vector3(3, 2, 0));

    /* Creacion de las opciones del menu */
    for(int i = 0; i < 4; i++){
        /* Cogemos el nombre de la que sera la opcion a crear */
        act_name << sn_names[i];

        /* La agregamos al scenenode del menu */
        nodoAux = _opNode->createChildSceneNode(act_name.str(), 
            Ogre::Vector3(0, (3 - i), 0));

        /* Creamos el nombre del mesh */
        act_name << ".mesh";

        /* Lo agregamos al nodo creado */
        ent = _sceneMgr->createEntity(act_name.str());
        nodoAux->attachObject(ent);

        /* Guardamos valores y reiniciamos variables de apoyo */
        _options[i] = nodoAux;
        act_name.str("");
    }//Fin for

    /* Puntero */
    nodoAux = _opNode->createChildSceneNode("Pointer", 
        Ogre::Vector3(-1, 3.1, 0));
    ent = _sceneMgr->createEntity("Pointer.mesh");
    nodoAux->attachObject(ent);

    _anim = ent->getAnimationState("Apuntar");
    _anim->setEnabled(true);
    _anim->setTimePosition(0);
    _anim->setLoop(true);

    _animRotar = ent->getAnimationState("Rotar");
    _animRotar->setEnabled(false);
    _animRotar->setTimePosition(0);
    _animRotar->setLoop(false);

    /* Background */
    nodoAux = _sceneMgr->getRootSceneNode()->createChildSceneNode("BMenu", 
        Ogre::Vector3(-0.5, 0, -9));
    ent = _sceneMgr->createEntity("Escenario.mesh");
    nodoAux->pitch(Ogre::Degree(90));
    nodoAux->attachObject(ent);
}//Fin createScene

void MenuState::createOverlay(){
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();
    _ovAbout = _overlayManager->getByName("About");
    _ovRecords = _overlayManager->getByName("Records");
}//Fin createOverlay

void MenuState::exit(){
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}//Fin exit

void MenuState::pause (){}

void MenuState::resume (){}

bool MenuState::frameStarted(const Ogre::FrameEvent& evt) {
    if(_ovRecords->isVisible()){
        showRecords();
    }//Fin if

    _anim->addTime(evt.timeSinceLastFrame);

    if(_animRotar->getEnabled()){
    	if(_animRotar->hasEnded()){
    		_animRotar->setEnabled(false);
    	}else{
    		_animRotar->addTime(evt.timeSinceLastFrame);
    	}
    }
    return true;
}//Fin frameStarted

bool MenuState::frameEnded(const Ogre::FrameEvent& evt){
    if(_exitGame){
        std::cout << "Guardando Records..." << std::endl;
        RecordManager::getSingletonPtr()->saveRecords();
        return false;
    }//Fin if
  
    return true;
}//Fin if

void MenuState::menuActions(){
    switch(_select){
        case 0:
        	_pTrackManager->load("arkanoid.mp3")->stop();
            changeState(PlayState::getSingletonPtr());
            break;
        case 1:
            _inOption = true;
            _opNode->setVisible(false);
            _ovRecords->show();
            break;
        case 2:
            _inOption = true;
            _opNode->setVisible(false);
            _ovAbout->show();
            break;
        case 3:
            _exitGame = true;
        break;
    }//Fin switch
}//Fin menuActions

void MenuState::showRecords(){
    Ogre::OverlayElement *oe;
    oe = _overlayManager->getOverlayElement("RecordsText");
    oe->setCaption(RecordManager::getSingletonPtr()->toString());
}//Fin showRecords

void MenuState::keyPressed(const OIS::KeyEvent &e){
    /* Valores maximos y minimos */
    Ogre::Real min = 0.1;
    Ogre::Real max = 3.1;

    /* Cogemos el scenenode del puntero */
    Ogre::Node* p = _opNode->getChild("Pointer");
    /* Posicion actual del puntero */
    Ogre::Vector3 pos = p->getPosition();

    switch(e.key){
        case OIS::KC_UP:
            if(!_inOption){
                _simpleEffect->play();
                //_pSoundFXManager->load("FXMovPointer.ogg")->play();
                _animRotar->setTimePosition(0);
                _animRotar->setEnabled(true);

                if((pos[1] + 1) <= max){
                    _select -= 1;
                    p->setPosition(pos[0], (pos[1] + 1), pos[2]);
                } else {
                    _select = 3;
                    p->setPosition(pos[0], min, pos[2]);
                }//Fin if_else
            }//Fin if
            break;
        case OIS::KC_DOWN:
            if(!_inOption){
                _simpleEffect->play();
                //_pSoundFXManager->load("FXMovPointer.ogg")->play();
                _animRotar->setTimePosition(0);
                _animRotar->setEnabled(true);
                
                if((pos[1] - 1) > 0.09){
                    _select += 1;
                    p->setPosition(pos[0], (pos[1] - 1), pos[2]);
                } else {
                    _select = 0;
                    p->setPosition(pos[0], max, pos[2]);
                }//Fin if_else
            }//Fin if
            break;
        case OIS::KC_SPACE:
            if(!_inOption){
                menuActions();
            }//Fin if
            break;
        case OIS::KC_ESCAPE:
            if(_inOption){
                _inOption = false;
                if(_ovAbout->isVisible()){
                    _ovAbout->hide();
                    _opNode->setVisible(true);
                } else {
                    _ovRecords->hide();
                    _opNode->setVisible(true);
                }//Fin if-else
            }//Fin if
            break;
        default:
            /* Cualquier otra tecla no hace nada */
            break;
    }//Fin switch
}//Fin keyPressed

void MenuState::keyReleased(const OIS::KeyEvent &e ){}

MenuState* MenuState::getSingletonPtr (){
    return msSingleton;
}//Fin getSingletonPtr

MenuState& MenuState::getSingleton (){ 
    assert(msSingleton);
    return *msSingleton;
}//Fin getSingleton
