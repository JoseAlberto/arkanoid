#ifndef MenuState_H
#define MenuState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include "TrackManager.h"
#include "SoundFXManager.h"
#include "GameState.h"

class MenuState : public Ogre::Singleton<MenuState>, public GameState{
    public:
        MenuState() {}

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        /* Heredados de Ogre::Singleton. */
        static MenuState& getSingleton ();
        static MenuState* getSingletonPtr ();

    protected:
        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;
        Ogre::OverlayManager* _overlayManager;

        Ogre::Overlay *_ovAbout, *_ovRecords;

        SoundFXManager* _pSoundFXManager;
        SoundFXPtr _simpleEffect;
        TrackManager * _pTrackManager;

        Ogre::AnimationState* _anim;
        Ogre::AnimationState* _animRotar;

        bool _exitGame, _inOption;

    private:
        int _select;
        Ogre::SceneNode* _opNode;
        Ogre::SceneNode** _options;

        void createScene();
        void createOverlay();
        void menuActions();
        void showRecords();
};

#endif
