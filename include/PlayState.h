#ifndef PlayState_H
#define PlayState_H

#include <vector>
#include <Ogre.h>
#include <OIS/OIS.h>

#include "Bloque.h"
#include "GameState.h"
#include "TrackManager.h"
#include "SoundFXManager.h"

class PlayState : public Ogre::Singleton<PlayState>, public GameState {
    public:
        PlayState(){}

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        void finPartida ();

        /* Heredados de Ogre::Singleton */
        static PlayState& getSingleton ();
        static PlayState* getSingletonPtr ();

    protected:
        Ogre::Root* _root;
        Ogre::Camera* _camera;
        Ogre::Viewport* _viewport;
        Ogre::SceneManager* _sceneMgr;
        Ogre::OverlayManager* _overlayManager;

        Ogre::Overlay *_ovPlay;
        Ogre::OverlayElement *_o_score;

        Ogre::SceneNode* _nave;
        Ogre::SceneNode* _bola;
        Ogre::SceneNode* _disparos;
        Ogre::SceneNode* _bloqueTecho;
        Ogre::SceneNode* _bloqueParedDer;
        Ogre::SceneNode* _bloqueParedIzq;
        Ogre::SceneNode* _blqEstructura;

        SoundFXManager* _pSoundFXManager;
        SoundFXPtr _simpleEffect, _capturar, _metal;
        TrackManager* _pTrackManager;


        Bloque** _bloques, *_actual;
        Ogre::Vector3 _movBola, _movDisparo, _movBoost;

        std::vector<Ogre::SceneNode*> _power;
        std::vector<Ogre::AnimationState*> _anim;
        std::vector<Bloque*> _bts, _mbts, _actblq;

        int _nBloq, _x, _y, _estadoNave, _score, _vidas, _nivel;
        Ogre::Real _nSpeed, _bSpeed, _dSpeed, _btSpeed, _deltaT;
        bool _endGame, _endPGame, _izqPres, _derPres, _iniJuego, _disparar, 
        _disparando, _controlar, _bolaCapturada, _combo, _endLevel;
        
        int **_posiciones;


    private:
        void chocarNave();
        void updateAnim();
        void createScene();
        void createOverlay();
        void updateBoost();
        void chocarParedes();
        void chocarBloques();
        void crearBoosters();
        void crearBloques();
        void actualizarScore();
        void activarBooster(Bloque *b);
        Ogre::Vector3 limitesNave();
        void activarAnimacion(int c, int f, int animacion);
        void destroyAllAttachedMovableObjects(Ogre::SceneNode* node);
        void limpiar();
        void inicializar();
        void MatrizPosicion();

};

#endif
