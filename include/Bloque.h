#ifndef BLOQUE
#define BLOQUE

#include <Ogre.h>

class Bloque {
	public:
		Bloque();
		/* Metodos de consulta */
		int getTipo() const;
		int getVida() const;
		Ogre::SceneNode* getNodo() const;
		Ogre::Vector3 getPosicion() const;
		int getCara(Ogre::Vector3 p) const;
		Ogre::SceneNode* getBooster() const;
		int getBoosterType() const;
		int getPosX() const;
		int getPosY() const;
		bool getActivo() const;
		bool getGolpeado() const;
		bool hasEnabledAnimations();
		/* Metodos de Modificacion */
		void setPosCaras();
		void setFlag(int i);
		void setTipo(int t);
		void setVida(int v);
		void setNodo(Ogre::SceneNode *n);
		void setPosicion(Ogre::Vector3 p);
		void setBooster(Ogre::SceneNode *b);
		void setBoosterType(int n_b);
		void setPosX(int posicion);
		void setPosY(int posicion);
		void setActivo(bool act);
		void setGolpeado(bool gol);
		void setAnimationState(Ogre::AnimationState *a);
	private:
		int _tipo;
		int _vida;
		int _btsType;
		bool _activo;
		bool _golpeado;
		int _posX, _posY;
		static int _flags[8];

		Ogre::Vector3 _pos;
		Ogre::Vector3 *_caras;
		Ogre::SceneNode *_nodo, *_booster;

		std::vector<Ogre::AnimationState*> _anims;
};

#endif
