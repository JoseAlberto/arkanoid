#include "PauseState.h"
#include "PlayState.h"

template<> PauseState* Ogre::Singleton<PauseState>::msSingleton = 0;

void PauseState::enter (){
    _root = Ogre::Root::getSingletonPtr();

    /* Se recupera el gestor de escena y la cámara. */
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("CamaraPrincipal");
    _viewport = _root->getAutoCreatedWindow()->getViewport(0);

    _pSoundFXManager=SoundFXManager::getSingletonPtr();

    createScene();

    _exitGame = false;
}//Fin enter

void PauseState::createScene(){
    /* Variables que vamos a usar */
    Ogre::Entity *ent = NULL;
    Ogre::SceneNode *nodo = NULL;
    Ogre::SceneNode *nodopadre = NULL;

    std::stringstream act_name;
    Ogre::String sn_names[] = {"Continue", "BMenu"};

    _pSoundFXManager->load("FXMovPointer.ogg");

    /* Iniciamos la variable de las opciones */
    _select = 0;
    _options = new Ogre::SceneNode*[2];

    nodopadre = _sceneMgr->getRootSceneNode()->createChildSceneNode(
        "NodoPausa", Ogre::Vector3(0, 23.5, 17));
    nodopadre->pitch(Ogre::Degree(35));

    /* Creacion de las opciones del submenu */
    for(int i = 0; i < 2; i++){
        /* Cogemos el nombre de la que sera la opcion a crear */
        act_name << sn_names[i];

        /* La agregamos al scenenode del root */
        nodo = nodopadre->createChildSceneNode(
        act_name.str(), Ogre::Vector3(-3.25, 3, (0 + i)));

        /* Creamos el nombre del mesh */
        act_name << ".mesh";

        /* Lo agregamos al nodo creado */
        ent = _sceneMgr->createEntity(act_name.str());
        nodo->attachObject(ent);

        /* Guardamos valores y reiniciamos variables de apoyo */
        _options[i] = nodo;
        act_name.str("");
    }//Fin for

    nodo = nodopadre->createChildSceneNode(
        "Pointer", Ogre::Vector3(-4, 3, 0.1));
    ent = _sceneMgr->createEntity("Pointer.mesh");
    nodo->setScale(0.8,0.8,0.8);
    nodo->attachObject(ent);
    nodo->pitch(Ogre::Degree(-90));

    _anim = ent->getAnimationState("Apuntar");
    _anim->setEnabled(true);
    _anim->setTimePosition(0);
    _anim->setLoop(true);

    _animRotar = ent->getAnimationState("Rotar");
    _animRotar->setEnabled(false);
    _animRotar->setTimePosition(0);
    _animRotar->setLoop(false);
}//Fin createOverlay

void PauseState::exit (){
    _sceneMgr->getRootSceneNode()->removeAndDestroyChild("NodoPausa");
}//Fin exit

void PauseState::pause (){}

void PauseState::resume (){}

bool PauseState::frameStarted(const Ogre::FrameEvent& evt){
	_anim->addTime(evt.timeSinceLastFrame);

	if(_animRotar->getEnabled()){
		if(_animRotar->hasEnded()){
			_animRotar->setEnabled(false);
		}else{
			_animRotar->addTime(evt.timeSinceLastFrame);
		}
	}
    return true;
}//Fin frameStarted

bool PauseState::frameEnded(const Ogre::FrameEvent& evt){
    if(_exitGame){
        return false;
    }//Fin if
  
    return true;
}//Fin frameEnded

void PauseState::menuActions(){
    switch(_select){
        case 0:
            popState();
            break;
        case 1:
            PlayState::getSingletonPtr()->finPartida();
            popState();
            break;
        break;
    }//Fin switch
}//Fin menuActions

void PauseState::keyPressed(const OIS::KeyEvent &e){
    /* Valores maximos y minimos */
    Ogre::Real min = 0.1;
    Ogre::Real max = 1.1;

    /* Cogemos el scenenode del puntero */
    Ogre::Node* p = _sceneMgr->getRootSceneNode()->getChild("NodoPausa")->getChild("Pointer");
    /* Posicion actual del puntero */
    Ogre::Vector3 pos = p->getPosition();

    switch(e.key){
        case OIS::KC_UP:
        	_pSoundFXManager->load("FXMovPointer.ogg")->play();
        	_animRotar->setTimePosition(0);
        	_animRotar->setEnabled(true);

            if((pos[2] - 1) > 0.09){
                _select = 0;
                p->setPosition(pos[0], pos[1], (pos[2] - 1));
            } else {
                _select = 1;
                p->setPosition(pos[0], pos[1], max);
            }//Fin if_else
            break;
        case OIS::KC_DOWN:
        	_pSoundFXManager->load("FXMovPointer.ogg")->play();
        	_animRotar->setTimePosition(0);
        	_animRotar->setEnabled(true);

            if((pos[2] + 1) <= max){
                _select = 1;
                p->setPosition(pos[0], pos[1], (pos[2] + 1));
            } else {
                _select = 0;
                p->setPosition(pos[0], pos[1], min);
            }//Fin if_else
            break;
        case OIS::KC_SPACE:
                menuActions();
            break;
        default:
            /* Cualquier otra tecla no hace nada */
            break;
    }//Fin switch 
}//Fin keyPressed

void PauseState::keyReleased(const OIS::KeyEvent &e){}

PauseState* PauseState::getSingletonPtr (){
    return msSingleton;
}//Fin getSingletonPtr

PauseState& PauseState::getSingleton (){ 
    assert(msSingleton);
    return *msSingleton;
}//Fin getSingleton
