#include "PlayState.h"
#include "MenuState.h"
#include "PauseState.h"
#include "EndState.h"
#include "Boosters.h"

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

void PlayState::enter(){
    /* Recuperamos el root */
    _root = Ogre::Root::getSingletonPtr();

    _pSoundFXManager = SoundFXManager::getSingletonPtr();

    /* Se recupera el gestor de escena y la cámara */
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("CamaraPrincipal");
    
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    /* Creamos el conjunto de bloques */
    _x = 14; _y = 14;
    _bloques = new Bloque*[_x];
    for(int i = 0; i < _x; i++){ _bloques[i] = new Bloque[_y];}

    _posiciones = new int*[_x];
    for(int i = 0; i<_x; i++){_posiciones[i] = new int[_y];}

     _nivel = 0;
    MatrizPosicion();

    /* Inicialización tamaño de la nave*/
    _estadoNave = NAVE_NORMAL;

    /* Reservamos memoria para los boosters */
    _power.reserve(3);
    _actblq.reserve(_nBloq);
    _bts.reserve(NUMERO_BOOSTERS);
    _mbts.reserve(NUMERO_BOOSTERS);
    _anim.reserve(_nBloq + NUMERO_BOOSTERS);

    _pSoundFXManager = SoundFXManager::getSingletonPtr();
    _pTrackManager = TrackManager::getSingletonPtr();

    _simpleEffect = _pSoundFXManager->load("ChoqueCaja.ogg");
    _capturar = _pSoundFXManager->load("CapturarBooster.ogg");
    _metal = _pSoundFXManager->load("ChoqueMetal.ogg");

    /* Creamos la escena */
    createScene();
    /* Activamos los overlays */
    createOverlay();

    _score = 0; _vidas = 3;
    _btSpeed = 5.0; _nSpeed = 15.0; 
    _dSpeed = 25.0; _bSpeed = 8.0;
    _movBoost = Ogre::Vector3(0, 0, 1);
    _movBola = Ogre::Vector3(1, 0, -1);
    _movDisparo = Ogre::Vector3(0, 0, -1);
    _endLevel = _endPGame = false;
    _disparar = _disparando = _controlar = _bolaCapturada = false;
    _combo = _endGame = _izqPres = _derPres = _iniJuego = false;
}//Fin enter

void PlayState::createScene() {
    Ogre::Entity *ent = NULL;
    Ogre::SceneNode *nodo = NULL;

    std::stringstream sn;
    _pTrackManager->load("PlayTrack.ogg")->play();
    /* Posicionamos la camara */
    _camera->setPosition(Ogre::Vector3(0, 32, 24));
    _camera->lookAt(Ogre::Vector3(0, 0, -2));

    /*  
        >>>>>>>>>>> Posible easter-egg <<<<<<<<<<<
        _camera->setPosition(Ogre::Vector3(0, 32, -24));
        >>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<
    */

    /* Base */
    nodo = _sceneMgr->getRootSceneNode()->createChildSceneNode(
        "Escenario", Ogre::Vector3(0, 0, 0));
    ent = _sceneMgr->createEntity("Escenario.mesh");
    nodo->attachObject(ent);
    /* Pared Izquierda */
    _bloqueParedIzq = _sceneMgr->getRootSceneNode()->createChildSceneNode(
        "ParedIzq", Ogre::Vector3(-14.5, 1, 0));
    ent = _sceneMgr->createEntity("Pared.mesh");
    _bloqueParedIzq->attachObject(ent);
    /* Pared Derecha */
    _bloqueParedDer = _sceneMgr->getRootSceneNode()->createChildSceneNode(
        "ParedDer", Ogre::Vector3(14.5, 1, 0));
    ent = _sceneMgr->createEntity("Pared.mesh");
    _bloqueParedDer->attachObject(ent);
    /* Techo */
    _bloqueTecho = _sceneMgr->getRootSceneNode()->createChildSceneNode(
        "ParedTech", Ogre::Vector3(0, 1, -12.5));
    ent = _sceneMgr->createEntity("Pared_Top.mesh");
    _bloqueTecho->attachObject(ent);

    /* Nave */
    _nave = _sceneMgr->getRootSceneNode()->createChildSceneNode(
        "Nave", Ogre::Vector3(0, 0.5, 11.5));
    ent = _sceneMgr->createEntity("Nave.mesh");
    _nave->attachObject(ent);
    _nave->setInitialState();

    /* Bola */
    _bola = _sceneMgr->getRootSceneNode()->createChildSceneNode(
        "Bola", Ogre::Vector3(0, 1, 10.99));
    ent = _sceneMgr->createEntity("Bola.mesh");
    _bola->attachObject(ent);
    _bola->setInitialState();

    /*Disparos*/
    _disparos = _sceneMgr->getRootSceneNode()->createChildSceneNode(
    		"Disparo", _nave->getPosition());
    ent =_sceneMgr->createEntity("3_Boost.mesh");
    ent->setMaterialName("M_3_Boost_Material");
    _disparos->yaw(Ogre::Degree(90), Ogre::Node::TS_LOCAL);
    _disparos->setScale(0.5, 0.5, 0.5);
    _disparos->attachObject(ent);
    _disparos->setVisible(false);
    _disparos->setInitialState();

    /* Vidas */
    int aux = -13;
    for(int i = 0; i < 3; i++){
        sn << "Power_" << i;
        nodo = _sceneMgr->getRootSceneNode()->createChildSceneNode(
            (sn.str()),Ogre::Vector3(aux, 0, 13));
        ent = _sceneMgr->createEntity("Nave.mesh");
        nodo->setScale(0.5, 0.5, 0.5);
        nodo->attachObject(ent);
        _power.push_back(nodo);

        aux += 2; sn.str("");
    }//Fin for

    _blqEstructura = _sceneMgr->getRootSceneNode()->createChildSceneNode(
        "Bloques", Ogre::Vector3(0, 0, 0));


    /* Creamos los bloques */
    crearBloques();

    /* Creamos los boosters */
    crearBoosters();

    /* Plano del suelo */
    Ogre::Plane plane(Ogre::Vector3::UNIT_Y, -0.001);
    Ogre::MeshManager::getSingleton().createPlane("plane",
    Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane,
    400, 400, 1, 1, true, 1, 20, 20, Ogre::Vector3::UNIT_Z);
    Ogre::SceneNode* nod_suelo = _sceneMgr->getRootSceneNode()->createChildSceneNode("Ground");
    Ogre::Entity* groundEnt = _sceneMgr->createEntity("planeEnt", "plane");
    groundEnt->setMaterialName("Ground");
    groundEnt->setCastShadows(false);
    nod_suelo->attachObject(groundEnt);

    /* Sombras */
    _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_MODULATIVE);
    _sceneMgr->setShadowColour(Ogre::ColourValue(0.5, 0.5, 0.5) );
    _sceneMgr->setAmbientLight(Ogre::ColourValue(0.9, 0.9, 0.9));
    _sceneMgr->setShadowTextureCount(2);
    _sceneMgr->setShadowTextureSize(512);

    /* Iluminacion */
    Ogre::Light *light = _sceneMgr->createLight("Light");
    light->setType(Ogre::Light::LT_SPOTLIGHT);
    light->setDirection(Ogre::Vector3(0,-1,0));
    light->setSpotlightInnerAngle(Ogre::Degree(25.0f));
    light->setSpotlightOuterAngle(Ogre::Degree(200.0f));
    light->setPosition(0, 150, 0);
    light->setSpecularColour(1, 1, 1);
    light->setDiffuseColour(1, 1, 1);
    light->setSpotlightFalloff(5.0f);
    light->setCastShadows(true);
}//Fin createScene

void PlayState::createOverlay(){
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();
    _ovPlay = _overlayManager->getByName("Play");
    _o_score = _overlayManager->getOverlayElement("PlayText");
    _ovPlay->show();
}//Fin createOverlay

void PlayState::exit(){
    for(int i = 0; i < _x; i++){ 
        delete [] _bloques[i]; 
        delete [] _posiciones[i];
    }//Fin for

    delete [] _bloques;
    delete [] _posiciones;

    _bts.clear();
    _actblq.clear();
    _anim.clear();
    _mbts.clear();
    _power.clear();

    _actual = NULL;

    _pTrackManager->load("PlayTrack.ogg")->stop();
    _ovPlay->hide();
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}//Fin exist

void PlayState::pause(){}
void PlayState::resume(){
    if(_endGame){
        changeState(MenuState::getSingletonPtr());
    }//Fin if
}//Fin resume

bool PlayState::frameStarted(const Ogre::FrameEvent& evt){
    Ogre::Vector3 vn(0, 0, 0);
    _deltaT = evt.timeSinceLastFrame;

    if(!_endGame || !_endLevel || !_endPGame){
        /* Movimiento Nave */
   	    vn += limitesNave();
        _nave->translate(vn * _deltaT * _nSpeed);

        /* Movimiento Bola */
        if(!_iniJuego || (_controlar && _bolaCapturada)){
            _bola->translate(vn * _deltaT * _nSpeed);
        } else {
    	   _bola->translate(_bola->getOrientation() * _movBola * _deltaT * _bSpeed);
        }//Fin if-else

        /* Movimiento Disparo */
        if(_disparar && _disparando){
            _disparos->translate(_movDisparo * _deltaT * _dSpeed);
        } else {
            _disparos->translate(vn * _deltaT * _nSpeed);
        }//Fin if-else

        /* Comprobacion de choque con nave */
        chocarNave();
        /* Comprobación de choque con paredes, techo y nave*/
        chocarParedes();
        /* Comprobacion de choque con bloques */
        chocarBloques();
    }//Fin if

    /* Movimiento animaciones */
    updateAnim();
    /* Movimiento boosters */
    updateBoost();

    /* Si la bola se pierde, reiniciamos estados */
    if(_bola->getPosition().z > 12){
        _nave->resetToInitialState();
        _bola->resetToInitialState();
        _disparos->resetToInitialState();
        _disparos->setVisible(false);
        
        _estadoNave = NAVE_NORMAL;
        _nSpeed = 15.0; _bSpeed = 8.0;
        _movBola = Ogre::Vector3(1, 0, -1);
        _iniJuego = _controlar = _disparar = false;

        _vidas--;
        _power[_vidas]->setVisible(false);

        if(_vidas == 0){
            _endGame = true;
        }//Fin if
    }//Fin if

    /* Si no quedan mas bloques, pasamos al EndState */
    if(_nBloq == 0 && !_endLevel){
        _nivel++;

    	if(_nivel == 3){
            _endGame = true;
        }//Fin if
    	_endLevel = true;
    }//Fin if

    /* Mostramos puntuacion */
    if(_ovPlay->isVisible()){
        actualizarScore();
    }//Fin if
    return true;
}//Fin frameStarted

bool PlayState::frameEnded(const Ogre::FrameEvent& evt){  
	if(_endGame && _anim.size() == 0){
        _ovPlay->hide();
        EndState::getSingletonPtr()->addScore(_score);
	    pushState(EndState::getSingletonPtr());
	} else if(_endPGame && _anim.size() == 0){
        changeState(MenuState::getSingletonPtr());
    } else if(_endLevel && _anim.size() == 0){
        limpiar();
        _endLevel = false;
    }//Fin if-else
    return true;
}//Fin frameEnded

void PlayState::chocarNave(){
    int tPor;
    Ogre::Vector3 nRight, nLeft;
    Ogre::AxisAlignedBox bboxBola = _bola->_getWorldAABB();
    Ogre::AxisAlignedBox bboxNave = _nave->_getWorldAABB();

    if(bboxBola.intersects(bboxNave)){
        nLeft = bboxNave.getCorner(Ogre::AxisAlignedBox::FAR_LEFT_BOTTOM);
        nRight = bboxNave.getCorner(Ogre::AxisAlignedBox::FAR_RIGHT_BOTTOM);

        if(_controlar){
            _bolaCapturada = true;
            /* Para evitar nuevo choque de bb */
            _bola->translate(0, 0, -0.2);
        } else {
            tPor = ((_bola->getPosition().x - nLeft.x) * 100) / (nRight.x - nLeft.x);

            _movBola.z = -1;
            if(tPor <= 30){
                _movBola.x = -1;
            } else if(tPor >= 70){
                _movBola.x = 1;
            }//Fin if-else
        }//Fin if-else

        /* Cambiamos flag si esta activado */
        if(_combo){
            _combo = false;
        }//Fin if

        if(_actual){
            _actual->setGolpeado(false);
        }//Fin if
    }//Fin if
}//Fin chocarNave

void PlayState::chocarParedes(){
    Ogre::AxisAlignedBox bboxBola = _bola->_getWorldAABB();
    Ogre::AxisAlignedBox bboxNave = _nave->_getWorldAABB();
    Ogre::AxisAlignedBox bboxTecho = _bloqueTecho->_getWorldAABB();
    Ogre::AxisAlignedBox bboxParedI = _bloqueParedIzq->_getWorldAABB();
    Ogre::AxisAlignedBox bboxParedD = _bloqueParedDer->_getWorldAABB();

    if(bboxBola.intersects(bboxTecho)){
        _movBola.z = 1;

        if(_actual){
            _actual->setGolpeado(false);
        }//Fin if
    }//Fin if

    if(bboxBola.intersects(bboxParedI)){
        _movBola.x = 1;

        if(_actual){
            _actual->setGolpeado(false);
        }//Fin if
    }//Fin if

    if(bboxBola.intersects(bboxParedD)){
        _movBola.x = -1;

        if(_actual){
            _actual->setGolpeado(false);
        }//Fin if
    }//Fin if
}//Fin chocarParedes

void PlayState::chocarBloques(){
    int f, c;
    bool flag = false;
    Ogre::AxisAlignedBox bboxBloque, bboxDisparo;
    Ogre::AxisAlignedBox bboxBola = _bola->_getWorldAABB();

    for(int i = 0; (unsigned)i < _actblq.size(); i++){

        f = _actblq[i]->getPosY(); 
        c = _actblq[i]->getPosX();
        bboxBloque = _actblq[i]->getNodo()->_getWorldAABB();

        if(bboxBola.intersects(bboxBloque)){
            if(_bloques[c][f].getVida() > 0 && !_bloques[c][f].getGolpeado()){

                /* Cambiamos vector de movimiento de la bola en funcion de donde colisione */
                switch(_bloques[c][f].getCara(bboxBola.getCenter())){
                    case 0:
                        /* Frontal */
                        _movBola.z *= -1;
                        break;
                    case 1:
                        /* Izquierda */
                        _movBola.x *= -1;
                        break;
                    case 2:
                        /* Techo */
                        _movBola.z *= -1;
                        break;
                    case 3:
                        /* Derecha */
                        _movBola.x *= -1;
                        break;
                    case 4:
                        /* Pico inferior derecho */
                        if(_movBola.x == -1 && _movBola.z == -1){
                            _movBola.x *= -1; _movBola.z *= -1;
                        } else if(_movBola.x == 1){
                            _movBola.z *= -1;
                        } else {
                            _movBola.x *= -1;
                        }//Fin if-else
                        break;
                    case 5:
                        /* Pico inferior izquierdo */
                        if(_movBola.x == 1 && _movBola.z == -1){
                            _movBola.x *= -1; _movBola.z *= -1;
                        } else if(_movBola.x == 1){
                            _movBola.x *= -1;
                        } else {
                            _movBola.z *= -1;
                        }//Fin if-else
                        break;
                    case 6:
                        /* Pico superior derecho */
                        if(_movBola.x == -1 && _movBola.z == 1){
                            _movBola.x *= -1; _movBola.z *= -1;
                        } else if(_movBola.x == 1){
                            _movBola.z *= -1;
                        } else {
                            _movBola.x *= -1;
                        }//Fin if-else
                        break;
                    case 7:
                        /* Pico superior izquierdo */
                        if(_movBola.x == 1 && _movBola.z == 1){
                            _movBola.x *= -1; _movBola.z *= -1;
                        } else if(_movBola.x == 1){
                            _movBola.x *= -1;
                        } else {
                            _movBola.z *= -1;
                        }//Fin if-else
                        break;
                }//Fin switch

                /* Activamos el flag golpeado */
                _bloques[c][f].setGolpeado(true);

                if(!_actual){
                    _actual = &(_bloques[c][f]);
                } else {
                    _actual->setGolpeado(false);
                    _actual = &(_bloques[c][f]);
                }//Fin if-else

                /* Activamos flag de colision */
                flag = true;
            }//Fin if
        } else if(_disparar){
            bboxDisparo = _disparos->_getWorldAABB();
            if(bboxDisparo.intersects(bboxBloque) && _bloques[c][f].getVida() > 0){
                /* Activamos flag de colision */
                flag = true;
                /* Reiniciamos disparo */
                _disparando = false;
                _disparos->setVisible(false);
                _disparos->setPosition(_nave->getPosition());
            } else if(bboxDisparo.intersects(_bloqueTecho->_getWorldAABB())){
                /* Reiniciamos disparo */
                _disparando = false;
                _disparos->setVisible(false);
                _disparos->setPosition(_nave->getPosition());
            }//Fin if-else
        }//Fin if-else

        if(flag){
        	flag = false;

        	/* Hacemos play del sonido correspondiente para el tipo de bloque */
        	if(_posiciones[c][f] == 1){
        	   _simpleEffect->play();
            } else {
        	   _metal->play();
            }//Fin if-else

            /* Le quitamos una vida al bloque */
            _bloques[c][f].setVida(_bloques[c][f].getVida() - 1);

            /* Si la vida de un bloque llega a 0 */
            if(_bloques[c][f].getVida() == 0){
                /* Quitamos un bloque de los totales */
                _nBloq--;

                /* Aumentamos el score */
                if(_combo){
                    _score += 2;
                } else {
                    _score++;
                }//Fin if-else

                /* Desbloqueamos adyacentes */
                if((f - 1) >= 0 && _posiciones[c][f - 1]){
                	if(!_bloques[c][f - 1].getActivo()){
                        _bloques[c][f - 1].setActivo(true);
                        _actblq.push_back(&(_bloques[c][f - 1]));
                    }//Fin if
                }//Fin ig
                if((f + 1) < _y && _posiciones[c][f + 1]){
                    if(!_bloques[c][f + 1].getActivo()){
                        _bloques[c][f + 1].setActivo(true);
                        _actblq.push_back(&(_bloques[c][f + 1]));
                    }//Fin if
                }//Fin if
                if((c - 1) >= 0){
                	if(_posiciones[c - 1][f]){
                		_bloques[c - 1][f].setFlag(1);
                		if(!_bloques[c - 1][f].getActivo()){
                			_bloques[c - 1][f].setActivo(true);
                			_actblq.push_back(&(_bloques[c - 1][f]));
                		}//fin if
                	}//Fin if
                }//Fin if
                if((c + 1) < _x){
                	if(_posiciones[c + 1][f]){
                		_bloques[c + 1][f].setFlag(3);
                		if(!_bloques[c + 1][f].getActivo()){
                			_bloques[c + 1][f].setActivo(true);
                			_actblq.push_back(&(_bloques[c + 1][f]));
                		}//Fin if
                	}//fin if
                }//Fin if

                /* Activamos animaciones */
                activarAnimacion(c, f, 1);

                if(_bloques[c][f].getBoosterType() > 0){
                    /* Deberia agregarse despues de la animacion */
                    _bloques[c][f].getBooster()->setVisible(true);
                    activarAnimacion(c, f, 3);
                    _mbts.push_back(&(_bloques[c][f]));
                }//Fin if

                /* Activamos el flag del combo si no lo esta ya */
                if(!_combo){
                    _combo = true;
                }//Fin if

                /* Salimos de la funcion */
                return;
            } else if(_bloques[c][f].getTipo() != 1){
                activarAnimacion(c, f, 2);
            }//Fin if-else
        } else if(!_bloques[c][f].hasEnabledAnimations()){
            /* Quitamos el bloque */
            _actblq.erase(_actblq.begin() + i); i--;
            /* Ocultamos el bloque */
            _bloques[c][f].getNodo()->setVisible(false);
        }//Fin if-else
    }//Fin for

}//Fin chocarBloques

void PlayState::actualizarScore(){
    std::stringstream sc;

    if(_score < 100){
        sc << "0";
        if(_score < 10){
            sc << "0";
        }//Fin if
    }//Fin if

    sc << _score;

    _o_score->setCaption(sc.str());
}//Fin actualizarScore

void PlayState::keyPressed(const OIS::KeyEvent &e){
	SoundFXPtr disparoSound= _pSoundFXManager->load("Disparo.ogg");
    switch(e.key){
        case OIS::KC_ESCAPE:
        	pushState(PauseState::getSingletonPtr());
        	break;
        case OIS::KC_SPACE:
            _iniJuego = true;
            break;
        case OIS::KC_RIGHT:
            _derPres = true;
            break;
        case OIS::KC_LEFT:
            _izqPres = true;
            break;
        case OIS::KC_A:
        	if(_disparar && !_disparando){
        		disparoSound->play();
        		_disparando = true;
        		_disparos->setVisible(true);
        	}//Fin if
        	break;
        case OIS::KC_UP:
        	if(_controlar && _bolaCapturada){
                _movBola.z *= -1;
        		_bolaCapturada = false;
            }//Fin if
        	break;
        case OIS::KC_I:
            std::cout << _nivel << std::endl;
            std::cout << _vidas << std::endl;
            break;
        case OIS::KC_L:
        	_nBloq=0;
        default:
            break;
    }//switch
}//Fin keyPressed

void PlayState::keyReleased(const OIS::KeyEvent &e){
    switch(e.key){
        case OIS::KC_RIGHT:
            _derPres = false;
            break;
        case OIS::KC_LEFT:
            _izqPres = false;
            break;
        default:
            break;
    }//switch
}//Fin keyReleased

void PlayState::finPartida(){
    _endPGame = true;
}//Fin finPartida

PlayState* PlayState::getSingletonPtr(){
    return msSingleton;
}//Fin getSingletonPtr

PlayState& PlayState::getSingleton(){ 
    assert(msSingleton);
    return *msSingleton;
}//Fin getSingleton

void PlayState::crearBoosters(){
	Ogre::Entity *ent = NULL;
    Ogre::SceneNode *nodo = NULL;
    Ogre::AnimationState *animState = NULL;
	std::stringstream snbooster, material;
    unsigned int idx, idy, booster = 0;

    /* Reinicio de semilla */
	srand(time(NULL));

    /* Mientras que no tengamos el numero de booster buscado */
    while(_bts.size() != NUMERO_BOOSTERS){
        idx = (unsigned)rand() % _x;
        idy = (unsigned)rand() % _y;
        booster = (unsigned)rand() % BOOSTERS_DIST + 1;

        //Si hay bloque
        if(_posiciones[idx][idy]){
            /* Si el booster que tiene es 0 (no se ha cambiado) */
            if(_bloques[idx][idy].getBoosterType() == 0){
                /* Asignamos el booster */
                _bloques[idx][idy].setBoosterType(booster);

                /* Creamos el nombre del booster */
                snbooster << "Booster_" << idx << "_" << idy;

                /* Creamos el booster */
                nodo = _blqEstructura->createChildSceneNode(
                    snbooster.str(), _bloques[idx][idy].getNodo()->getPosition());
                /* Agregamos el booster como un nuevo entity */
                ent = _sceneMgr->createEntity("3_Boost.mesh");
                ent->setVisible(false);
                material << "M_3_Boost_Material" << _bloques[idx][idy].getBoosterType();
                ent->setMaterialName(material.str());

                animState = ent->getAnimationState("AparecerDef");
                animState->setEnabled(false);
                animState->setTimePosition(0);
                animState->setLoop(false);
                _bloques[idx][idy].setAnimationState(animState);

                /* Lo agregamos al nodo correspondiente */
                nodo->attachObject(ent);
                nodo->setInitialState();

                /* Agregamos el booster al bloque */
                _bloques[idx][idy].setBooster(nodo);
                /* Agregamos el booster al vector de boosters */
                _bts.push_back(&(_bloques[idx][idy]));

                /* Reiniciamos strings */
                snbooster.str("");
                material.str("");
            }//Fin if
        }//fin if
    }//Fin while
}//Fin crearBoosters

Ogre::Vector3 PlayState::limitesNave(){
    int limite = 0;
    Ogre::Vector3 vt = Ogre::Vector3(0, 0, 0);

	switch(_estadoNave){
        case NAVE_GRANDE:
           limite = 11;
           break;
        case NAVE_NORMAL:
		   limite = 12;
	       break;
    }//Fin switch

	if(_derPres && _nave->getPosition().x < limite){
        vt.x = 1;
	} else if(_izqPres && _nave->getPosition().x > (-1 * limite)){
        vt.x = -1;
    }//Fin if-else

	return vt;
}//Fin limitesNave

void PlayState::updateAnim(){
    for(int i = 0; (unsigned) i < _anim.size(); i++){
        if(_anim[i]->hasEnded()){
            _anim.erase(_anim.begin() + i);
            i--;
        } else {
            _anim[i]->addTime(_deltaT);
        }//Fin if-else
    }//Fin for
}//Fin updateAnim

void PlayState::updateBoost(){
    Ogre::Entity *ent;
    Ogre::AxisAlignedBox bboxBooster;
    Ogre::AxisAlignedBox bboxNave = _nave->_getWorldAABB();

    for(int i = 0; (unsigned)i < _mbts.size(); i++){
        ent = static_cast<Ogre::Entity*>(_mbts[i]->getBooster()->getAttachedObject(0));

        if(ent->getAnimationState("AparecerDef")->hasEnded()){
            bboxBooster = _mbts[i]->getBooster()->_getWorldAABB();

            if(!bboxNave.intersects(bboxBooster)){

                if(_mbts[i]->getBooster()->getPosition().z > 12){
                    /* Ocultamos el booster */
                    _mbts[i]->getBooster()->setVisible(false);
                    _mbts[i]->getBooster()->resetToInitialState();

                    /* Lo eliminamos del vector */
                    _mbts.erase(_mbts.begin() + i);
                    i--;
                } else {
                    /* Lo transladamos en z */
                    _mbts[i]->getBooster()->translate(_movBoost * _deltaT * _btSpeed);
                }//Fin if-else
            } else {
                /* Ocultamos el booster */
            	_pSoundFXManager->load("CapturarBooster.ogg")->play();
                _mbts[i]->getBooster()->setVisible(false);
                _mbts[i]->getBooster()->resetToInitialState();

                /* Activamos sus efectos */
                activarBooster(_mbts[i]);

                /* Lo eliminamos del vector */
                _mbts.erase(_mbts.begin() + i);
                i--;
            }//Fin if-else
        }//Fin if
    }//Fin for
}//Fin updateAnim

void PlayState::activarAnimacion(int c, int f, int animacion){
    Ogre::Entity *ent = NULL;
    Ogre::AnimationState *animState = NULL;

    switch(animacion){
        case 1:
            ent = static_cast<Ogre::Entity*>(_bloques[c][f].getNodo()->getAttachedObject(0));
            animState = ent->getAnimationState("Reducir2");
            /* Activamos la animacion */
            animState->setEnabled(true);
            /* La agregamos al vector para actualizarla */
            _anim.push_back(animState);
            break;
        case 2:
            /* Reiniciarla en caso de haber terminado o si esta en ejecucion */
            ent = static_cast<Ogre::Entity*>(_bloques[c][f].getNodo()->getAttachedObject(0));
            animState = ent->getAnimationState("Temblar");

            if(!animState->getEnabled()){
                animState->setEnabled(true);
                _anim.push_back(animState);
            }//Fin if

            if(!animState->hasEnded()){
                animState->setTimePosition(0);
            } else {
                animState->setTimePosition(0);
                _anim.push_back(animState);
            }//Fin if-else
            break;
        case 3:
            ent = static_cast<Ogre::Entity*>(_bloques[c][f].getBooster()->getAttachedObject(0));
            animState = ent->getAnimationState("AparecerDef");
            animState->setEnabled(true);
            _anim.push_back(animState);
            break;
    }//Fin switch
}//Fin updateAnim

void PlayState::activarBooster(Bloque *b){
    switch(b->getBoosterType()){
        case CRECER:
            _nave->setScale(1.75, 1, 1);
            _estadoNave = NAVE_GRANDE;
            break;
        case LASER:
            _disparar = true;
            break;
        case CONTROL:
            _controlar = true;
            break;
        case VIDAS:
            if(_vidas < 3){
                _power[_vidas]->setVisible(true);
                _vidas++;
            }//Fin if
            break;
        case VELOCIDAD_BOLA :
            _bSpeed = 5.0;
            break;
    }//Fin switch
}//Fin capturarBoosters

void PlayState::crearBloques(){
    bool golpeable;
	int cuenta = 0, aux = -13;
	
    Ogre::Entity *ent = NULL;
    Ogre::SceneNode *nodo = NULL;
	std::stringstream sn, material;
    Ogre::AnimationState *animState = NULL;

	sn.str("");

	for(int c = 0; c < _x; c++){
		for(int f = 0; f < _y; f++){
			if(_posiciones[c][f]){
				cuenta++;

                /* Asignamos material especifico segun tipo bloque */
				if(_posiciones[c][f] == 3){
					material << "Bloque_Mat";
                } else{
					material << "Bloque_Mat" << _posiciones[c][f];
                }//Fin if-else

				sn << "Bloque_" << c << "_"<< f;

				nodo = _blqEstructura->createChildSceneNode(
						sn.str(), Ogre::Vector3(aux, 0.5, (f - 10)));
				ent = _sceneMgr->createEntity(sn.str(), "Bloque.mesh");
				ent->setMaterialName(material.str());

				animState = ent->getAnimationState("Reducir2");
				animState->setEnabled(false);
				animState->setTimePosition(0);
				animState->setLoop(false);
				_bloques[c][f].setAnimationState(animState);

				animState = ent->getAnimationState("Temblar");
				animState->setEnabled(false);
				animState->setTimePosition(0);
				animState->setLoop(false);
				_bloques[c][f].setAnimationState(animState);

				nodo->attachObject(ent);

                _bloques[c][f].setTipo(_posiciones[c][f]);
				_bloques[c][f].setVida(_posiciones[c][f]);
				_bloques[c][f].setNodo(nodo);
				_bloques[c][f].setPosicion(nodo->getPosition());
				_bloques[c][f].setPosCaras();
				_bloques[c][f].setPosX(c);
				_bloques[c][f].setPosY(f);

				golpeable = false;
				if(f == (_y - 1) || f == 0){
					golpeable = true;
                } else {
    				for(int i = (c - 1); i <= (c + 1) && !golpeable; i++){
    					if(i<14 && i>=0){
    						if(!_posiciones[i][f]){
    							golpeable = true;
                            }//Fin if
    					}//Fin if
    				}//Fin for

    				for(int j = (f - 1); j <= (f + 1) && !golpeable; j++){
    					if(j < 14 && j >= 0){
    						if(!_posiciones[c][j]){
    							golpeable = true;
                            }//Fin if
    					}//Fin if
    				}//Fin for
                }//Fin if

    			if(golpeable){
    				_bloques[c][f].setActivo(true);
    				_actblq.push_back(&(_bloques[c][f]));
    			}//Fin if

    			sn.str("");
    			material.str("");
			}//Fin if
		}//Fin for
		aux += 2;
	}//Fin for

	_nBloq = cuenta;
}//Fin crearBloques

void PlayState::destroyAllAttachedMovableObjects(Ogre::SceneNode* node){
   if(!node) return;

   Ogre::SceneNode::ObjectIterator itObject = node->getAttachedObjectIterator();

   while (itObject.hasMoreElements()){
      node->getCreator()->destroyMovableObject(itObject.getNext());
   }//Fin while

   Ogre::SceneNode::ChildNodeIterator itChild = node->getChildIterator();

   while (itChild.hasMoreElements()){
      Ogre::SceneNode* pChildNode = static_cast<Ogre::SceneNode*>(itChild.getNext());
      destroyAllAttachedMovableObjects(pChildNode);
   }//Fin while
}//Fin destroyAllAttachedMovableObjects

void PlayState::limpiar(){
	_nave->resetToInitialState();
	_bola->resetToInitialState();
	_disparos->resetToInitialState();

    for(int i = 0; i < _x; i++){ 
        delete [] _bloques[i];
    }//Fin for
    delete [] _bloques;

    destroyAllAttachedMovableObjects(_blqEstructura);
    _blqEstructura->removeAndDestroyAllChildren();

	_bts.clear();
	_actblq.clear();
	_mbts.clear();

    _actual = NULL;

    /* Reiniciamos valores */
	inicializar();

    /* Cambiamos de estructura */
	MatrizPosicion();
	
    /* Creamos los bloques */
    crearBloques();
    /* Creamos los boosters */
    crearBoosters();
}//Fin limpiar

void PlayState::inicializar(){
	_bloques = new Bloque*[_x];
	for(int i = 0; i < _x; i++){ _bloques[i] = new Bloque[_y];}

	_estadoNave = NAVE_NORMAL;

	_btSpeed = 5.0; _nSpeed = 15.0;
	_dSpeed = 25.0; _bSpeed = 8.0;
	_movBoost = Ogre::Vector3(0, 0, 1);
	_movBola = Ogre::Vector3(1, 0, -1);
	_movDisparo = Ogre::Vector3(0, 0, -1);
	_disparar = _disparando = _controlar = _bolaCapturada = false;
	_combo = _endGame = _izqPres = _derPres = _iniJuego = false;
}//Fin inicializar

void PlayState::MatrizPosicion(){
    int lcont = 0;
    std::string str;
    std::stringstream sstr;
    Ogre::StringVector tipos, filas, columna;
    std::ifstream file("formas_Matriz.txt");

    if(file.is_open()){
        while (getline(file, str)){
            sstr << str;

            if(lcont < (_x - 1)){
                sstr << "\n";
            }//Fin if
        }//Fin if

        tipos = Ogre::StringUtil::split((sstr.str()).c_str(), "==");
        filas = Ogre::StringUtil::split(tipos[_nivel], "\n");
        for(int i = 0; i < _x; i++){
            columna = Ogre::StringUtil::split(filas[i], ",");
            for(int j = 0; j < _y; j++){
                _posiciones[i][j] = Ogre::StringConverter::parseInt(columna[j]);
            }//Fin for
        }//Fin for

        file.close();
    }//Fin if
}//Fin MatrizPosicion