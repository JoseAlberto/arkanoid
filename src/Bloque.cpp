#include "Bloque.h"

int Bloque::_flags[8] = {1, 0, 1, 0, 1, 1, 1, 1};

Bloque::Bloque(){
	_tipo = 0;
	_vida = 0;
	_nodo = NULL;
	_booster = NULL;
	_pos = Ogre::Vector3(0.0);
	_btsType = 0;
	_activo = false;
	_golpeado = false;
	_posX = -1;
	_posY = -1;

	_anims.reserve(3);
}//Fin constructor

int Bloque::getTipo() const { 
	return _tipo; 
}//Fin getVida
int Bloque::getVida() const { 
	return _vida; 
}//Fin getVida
Ogre::SceneNode* Bloque::getNodo() const { 
	return _nodo; 
}//Fin getNodo
Ogre::Vector3 Bloque::getPosicion() const {
	return _pos;
}//Fin getPosicion
int Bloque::getCara(Ogre::Vector3 p) const {
	int opcion = 0;
	Ogre::Real distancia, aux;

	distancia = p.distance(_caras[0]);

	for(int i = 1; i < 8; i++){
		if(_flags[i]){
			aux = p.distance(_caras[i]);

			if(aux < distancia){
				opcion = i;
				distancia = aux;
			}//Fin if
		}//Fin if
	}//Fin for

	return opcion;
}//Fin getCara
Ogre::SceneNode* Bloque::getBooster() const{
	return _booster;
}//Fin getBooster
int Bloque::getBoosterType() const{
	return _btsType;
}//Fin getBoosterType
int Bloque::getPosX() const {
	return _posX;
}//Fin getPosX
int Bloque::getPosY() const {
	return _posY;
}//Fin getPosY
bool Bloque::getActivo() const{
	return _activo;
}//Fin getActivo
bool Bloque::getGolpeado() const{
	return _golpeado;
}//Fin getActivo
bool Bloque::hasEnabledAnimations(){
	bool flag = false;

	if(_anims.size() > 0){
		for(int i = 0; (unsigned)i < _anims.size(); i++){
			if(!_anims[i]->hasEnded()){
				flag = true;
				break;
			} else {
				_anims.erase(_anims.begin() + i);
				i--;
			}//Fin if-else
		}//Fin for
	}//Fin if

	return flag;
}//Fin hasEnabledAnimations

void Bloque::setTipo(int t){
	_tipo = t;
}//Fin setVida
void Bloque::setVida(int v){
	_vida = v;
}//Fin setVida
void Bloque::setFlag(int i){
	if(i < 4 && !_flags[i]){
		_flags[i] = 1;
	}//Fin if
}//Fin setFlag
void Bloque::setPosicion(Ogre::Vector3 p){
	_pos = p;
}//Fin setPosicion
void Bloque::setNodo(Ogre::SceneNode *n){
	_nodo = n;
}//Fin setNodo
void Bloque::setPosCaras(){
	_caras = new Ogre::Vector3[8];
	
	/* Frontal */
	_caras[0] = Ogre::Vector3(_pos.x, (_pos.y + 0.25), (_pos.z + 0.5));
	/* Izquierda */
	_caras[1] = Ogre::Vector3((_pos.x - 1), (_pos.y + 0.25), _pos.z);
	/* Trasera */
	_caras[2] = Ogre::Vector3(_pos.x, (_pos.y + 0.25), (_pos.z - 0.5));
	/* Derecha */
	_caras[3] = Ogre::Vector3((_pos.x + 1), (_pos.y + 0.25), _pos.z);
	/* Pico inferior derecho */
	_caras[4] = Ogre::Vector3((_pos.x + 1), (_pos.y + 0.25), (_pos.z + 0.5));
	/* Pico inferior izquierdo */
	_caras[5] = Ogre::Vector3((_pos.x - 1), (_pos.y + 0.25), (_pos.z + 0.5));
	/* Pico superior derecho */
	_caras[6] = Ogre::Vector3((_pos.x + 1), (_pos.y + 0.25), (_pos.z - 0.5));
	/* Pico superior izquierdo */
	_caras[7] = Ogre::Vector3((_pos.x - 1), (_pos.y + 0.25), (_pos.z - 0.5));
}//Fin calCaras
void Bloque::setBooster(Ogre::SceneNode *b) {
	_booster = b;
}//Fin setBooster
void Bloque::setBoosterType(int n_b) {
	_btsType = n_b;
}//Fin setBooster
void Bloque::setPosX(int posicion)  {
	_posX = posicion;
}//Fin setPosX
void Bloque::setPosY(int posicion)  {
	_posY = posicion;
}//Fin setPosY
void Bloque::setActivo(bool act){
	_activo = act;
}//Fin setActivo
void Bloque::setGolpeado(bool gol){
	_golpeado = gol;
}//Fin setActivo
void Bloque::setAnimationState(Ogre::AnimationState *a){
	_anims.push_back(a);
}//Fin setAnimationState