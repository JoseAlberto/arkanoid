#ifndef IntroState_H
#define IntroState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include "TrackManager.h"
#include "SoundFXManager.h"
#include "GameState.h"

class IntroState : public Ogre::Singleton<IntroState>, public GameState{
    public:
        IntroState() {}

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        /* Heredados de Ogre::Singleton. */
        static IntroState& getSingleton ();
        static IntroState* getSingletonPtr ();
    protected:
        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;
	Ogre::AnimationState *animState;
        TrackManager* _pTrackManager;




        bool _exitGame;
    private:
        void createScene();

};

#endif
