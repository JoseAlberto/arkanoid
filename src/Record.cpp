#include "Record.h"

Record::Record(): _gamer(""), _score(0), _position(0){}

void Record::setScore(int sc){
	_score = sc;
}//Fin setScore

void Record::setPosition(int pos){
	_position = pos;
}//Fin setPosition

void Record::setGamer(Ogre::String gm){
	_gamer = gm;
}//Fin setGamer

int Record::getScore() const{
	return _score;
}//Fin getScore

int Record::getPosition() const{
	return _position;
}//Fin getPosition

Ogre::String Record::getGamer() const{
	return _gamer;
}//Fin getGamer

Ogre::String Record::toString(){
	std::stringstream aux;

	aux << _position << ".\t\t\t\t\t\t"
	    << _gamer << "\t\t\t\t\t\t";

	if(_score < 100){
        aux << "0";
        if(_score < 10){
            aux << "0";
        }//Fin if
    }//Fin if

    aux << _score;

	return aux.str().c_str();
}//Fin toString