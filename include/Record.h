#ifndef Record_h
#define Record_h

#include <Ogre.h>

class Record{
public:
	Record();

	void setScore(int sc);
	void setPosition(int pos);
	void setGamer(Ogre::String gm);

	int getScore() const;
	int getPosition() const;
	Ogre::String getGamer() const;

	Ogre::String toString();
private:
	Ogre::String _gamer;
	int _score, _position;
};

#endif