#ifndef InputManager_H
#define InputManager_H

#include <Ogre.h>
#include <OIS/OIS.h>

/* Gestor para los eventos de entrada (teclado y ratón) */
class InputManager : public Ogre::Singleton<InputManager>, public OIS::KeyListener{
    public:
        InputManager ();
        virtual ~InputManager ();
  
        void initialise (Ogre::RenderWindow *renderWindow);
        void capture ();

        // Gestión de listeners.
        void addKeyListener (OIS::KeyListener *keyListener, const std::string& instanceName);

        void removeKeyListener (const std::string& instanceName);
        void removeKeyListener (OIS::KeyListener *keyListener);
  
        void removeAllListeners ();
        void removeAllKeyListeners ();
  
        void setWindowExtents (int width, int height);

        OIS::Keyboard* getKeyboard ();

        /* Heredados de Ogre::Singleton. */
        static InputManager& getSingleton ();
        static InputManager* getSingletonPtr ();

    private:
        bool keyPressed (const OIS::KeyEvent &e);
        bool keyReleased (const OIS::KeyEvent &e);
  
        OIS::InputManager *_inputSystem;
        OIS::Keyboard *_keyboard;

        std::map<std::string, OIS::KeyListener*> _keyListeners;
        std::map<std::string, OIS::KeyListener*>::iterator itKeyListener;
        std::map<std::string, OIS::KeyListener*>::iterator itKeyListenerEnd;
};

#endif