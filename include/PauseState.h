#ifndef PauseState_H
#define PauseState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include "TrackManager.h"
#include "SoundFXManager.h"
#include "GameState.h"

class PauseState : public Ogre::Singleton<PauseState>, public GameState{
    public:
        PauseState() {}

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        /* Heredados de Ogre::Singleton. */
        static PauseState& getSingleton ();
        static PauseState* getSingletonPtr ();

    protected:
        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;
        SoundFXManager* _pSoundFXManager;
        //TrackPtr _mainTrack;
        SoundFXPtr _simpleEffect;
        Ogre::AnimationState* _anim;
        Ogre::AnimationState* _animRotar;
        bool _exitGame;

    private:
        int _select;
        Ogre::SceneNode **_options;
        
        void createScene();
        void menuActions();
};

#endif
