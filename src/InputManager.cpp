#include "InputManager.h"

template<> InputManager* Ogre::Singleton<InputManager>::msSingleton = 0;

InputManager::InputManager ():
    _inputSystem(0),
    _keyboard(0)
{}//Fin constructor

InputManager::~InputManager(){
    if(_inputSystem) {
        if(_keyboard) {
            _inputSystem->destroyInputObject(_keyboard);
            _keyboard = 0;
        }//Fin if

        OIS::InputManager::destroyInputSystem(_inputSystem);

        _inputSystem = 0;

        /* Limpiar todos los listeners */
        _keyListeners.clear();
    }//Fi if
}//Fin Destructor

void InputManager::initialise(Ogre::RenderWindow *renderWindow){
    if(!_inputSystem){    
        size_t windowHnd = 0;
        OIS::ParamList paramList;
        std::ostringstream windowHndStr;

        renderWindow->getCustomAttribute("WINDOW", &windowHnd);
        
        windowHndStr << windowHnd;
        paramList.insert(std::make_pair(std::string("WINDOW"),
				    windowHndStr.str()));

        _inputSystem = OIS::InputManager::
        createInputSystem(paramList);

        _keyboard = static_cast<OIS::Keyboard*>
        (_inputSystem->createInputObject(OIS::OISKeyboard, true));
        _keyboard->setEventCallback(this);

        int left, top;
        unsigned int width, height, depth;
        renderWindow->getMetrics(width, height, depth, left, top);

        this->setWindowExtents(width, height);
    }//Fin if
}//Fin initialize

void InputManager::capture (){
    /* Capturar y actualizar cada frame */  
    if(_keyboard){
        _keyboard->capture();
    }//Fin if
}//Fin capture

void InputManager::addKeyListener(OIS::KeyListener *keyListener, const std::string& instanceName){
    if(_keyboard){
        /* Comprobar si el listener existe */
        itKeyListener = _keyListeners.find(instanceName);

        if(itKeyListener == _keyListeners.end()){
            _keyListeners[instanceName] = keyListener;
        }//Fin if
    }//Fin if
}//Fin addKeyListener

void InputManager::removeKeyListener(const std::string& instanceName){
    /* Comprobar si el listener existe */
    itKeyListener = _keyListeners.find(instanceName);

    if (itKeyListener != _keyListeners.end()) {
        _keyListeners.erase(itKeyListener);
    }//Fin if
}//Fin removeKeyListener

void InputManager::removeKeyListener(OIS::KeyListener *keyListener){
    itKeyListener = _keyListeners.begin();
    itKeyListenerEnd = _keyListeners.end();

    for (; itKeyListener != itKeyListenerEnd; ++itKeyListener) {
        if(itKeyListener->second == keyListener) {
            _keyListeners.erase(itKeyListener);
            break;
        }//Fin if
    }//Fin for
}//Fin removeKeyListener

void InputManager::removeAllListeners(){
    _keyListeners.clear();
}//Fin removeAllListeners

void InputManager::removeAllKeyListeners(){
    _keyListeners.clear();
}//Fin removeAllKelListeners

void InputManager::setWindowExtents(int width, int height){
}//Fin setWindowExtents

OIS::Keyboard* InputManager::getKeyboard(){
    return _keyboard;
}//Fin getKeyboard

bool InputManager::keyPressed(const OIS::KeyEvent &e){
    itKeyListener = _keyListeners.begin();
    itKeyListenerEnd = _keyListeners.end();

    /* Delega en los KeyListener añadidos */
    for (; itKeyListener != itKeyListenerEnd; ++itKeyListener) {
        itKeyListener->second->keyPressed(e);
    }//Fin for

    return true;
}//Fin keyPressed

bool InputManager::keyReleased(const OIS::KeyEvent &e){
    itKeyListener = _keyListeners.begin();
    itKeyListenerEnd = _keyListeners.end();

    /* Delega en los KeyListener añadidos */
    for (; itKeyListener != itKeyListenerEnd; ++itKeyListener) {
        itKeyListener->second->keyReleased(e);
    }//Fin for

    return true;
}//Fin keyReleased

InputManager* InputManager::getSingletonPtr(){
    return msSingleton;
}//Fin getSingletonPtr

InputManager& InputManager::getSingleton(){  
    assert(msSingleton);
    return *msSingleton;
}//Fin getSingleton